const fs = require('fs');
const puppeteer = require('puppeteer');
const mongoose = require('mongoose');
const express = require('express');
const cons = require('consolidate');
const bodyParser = require('body-parser')
const app = express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// app.set('views', ['views']);
app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile, cons.swig);
app.set('view engine', 'html');
app.use(express.static(__dirname + '/assets'));


// External files import
// const category = require('./controllers/category.js');
// const sub_category = require('./controllers/sub_category.js');
// const pagination = require('./controllers/pagination.js');
// const product_list = require('./controllers/product_list.js');
// const product_detail = require('./controllers/product_detail.js');

// Models
// const Product = require('./models/product.js');
// const Category = require('../models/category.js');

// connection port set
app.listen(8080, () => {
  console.log("Server is listening on port 8080");
});

require('mongodb').MongoClient;
var url = "mongodb://admin:admin123@159.89.55.35:27017/amazon"; // live
// const url = "mongodb://admin:admin123@ds261817.mlab.com:61817/amazon"; // mlab
mongoose.connect(url);

// Require routes
require('./routes/routes.js')(app);
