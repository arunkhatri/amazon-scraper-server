'use strict';

// External files import
const category = require('../controllers/category.js');
const sub_category = require('../controllers/sub_category.js');
const pagination = require('../controllers/pagination.js');
const product_list = require('../controllers/product_list.js');
const product_detail = require('../controllers/product_detail.js');
const scrapHelper = require('../helper/scraper');

// Models
const Product = require('../models/product.js');
const Category = require('../models/category.js');

module.exports = (app) => {

  app.get('/', async (req, res) => {
    console.log("Hiii Get Init", req.query);
    let products = [];
    let subcategories = [];
    const categories = await Category.distinct('category'); 
    const category = req.query.category ? req.query.category.replace('_', '&') : '';
    const subcategory =  req.query.subcategory ? req.query.subcategory.replace('_', '&') : '';
    const isProcess = scrapHelper.checkProcess();
    console.log('isProcess', isProcess)
    if(category){
      let query = {};
      if(category){
        query.category = category;
      }
      if(subcategory){
        query.subcategory = subcategory;
      }
      subcategories = await Category.distinct('subcategory', {category});
      products =  await Product.find(query);
      res.render('home', { products, categories, subcategories, category, subcategory, isProcess });
    } else {
      res.render('home', { categories, subcategories, products, category, subcategory, isProcess });
    }
  });

  app.get('/getsubcategory/:category', async (req, res) => {
    const category = req.params.category.replace('_', '&')
    const subcategory = await Category.find({category}, { subcategory: 1 });    
    res.send(subcategory);
  });

  app.post('/scrap', async (req, res) => {
    console.log('scrap', req.body);
    await scrapHelper.updateStop(false);
    const category = req.body.category.replace('_', '&');
    const subcategory = req.body.subcategory.replace('_', '&');
    scrapHelper.scrap({category, subcategory});
    const isProcess = scrapHelper.checkProcess();
    res.send({isProcess});
  });
  
  app.get('/restart', async (req, res) => {
    scrapHelper.updateStop(true);
    res.send(true);
  });

  app.get('/:category/:subcategory', async (req, res) => {
    // res.send('Server is listening on port 80');
    console.log('/:category/:subcategory'+ req.params)
    const { category, subcategory } = req.params;
    let query = {};
      if(category){
        query.category = category;
      }
      if(subcategory){
        query.subcategory = subcategory;
      }
    const categories = await Category.aggregate([    
      {
        $group : {
          _id: "$category",  
          subcategory: {
              $push : "$subcategory"
          }
        }
      }
    ]);
    Product.find(query, function(err, products){
      res.render('home', { products, categories, category, subcategory });
    });
  });
      
    
    app.get('/amazon/product/scrap', async (req, res) => {
        const data = await Category.find({});
        for (let i = 0; i < data.length; i++) {
          console.log('category', data[i].category, 'subcategory', data[i].subcategory)
          const cat = data[i].category; //'Computers'
          const s_cat = data[i].subcategory; //'COMPUTER COMPONENTS'
          let cat_url, sub_cat_url;
          if(cat){
            cat_url = await category.init('https://www.amazon.com', cat);
          }
          if(s_cat){
            sub_cat_url = await sub_category.init(cat_url, s_cat);
          }
          var url = cat && !s_cat ? cat_url : sub_cat_url;
          var flag = cat && !s_cat ? false : true;
          if (url) {
              var pages = await pagination.init(url, flag);
              for (const page_url of pages) {
              const products_list = await product_list.call(page_url, flag);
              await productDetail(products_list, req.query);
              }
          } else {
              console.log("url", url)
          }
        }
    });

    app.get('/product/detail/:id', async (req, res) => {
      // res.send('Server is listening on port 80');
      console.log(req.params)
      const { id } = req.params;
      let query = {};
      if(id){
        query._id = id;
      }
      Product.findOne(query,function(err, product_detail){
        res.render('product_detail', { product_detail });
      });
  });

  // app.get('/', async (req, res) => {  
  //   // res.send('Server is listening on port 80');
  //   const categories = await Category.aggregate([    
  //     {
  //       $group : {
  //         _id: "$category",  
  //         subcategory: {
  //             $push : "$subcategory"
  //         }
  //       }
  //     }
  //   ]);
  //   Product.find({}, function(err, products){
  //     res.render('index', { products, categories, category: '', subcategory: '' });
  //   });
  // });
}