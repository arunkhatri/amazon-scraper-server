const mongoose = require('mongoose');

const ProductSchema = mongoose.Schema({
    subcategory: String,
	category: String,
}, {
	timestamps: true
});

module.exports = mongoose.model('Category', ProductSchema);