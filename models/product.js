const mongoose = require('mongoose');

const ProductSchema = mongoose.Schema({
    title: String,
    price: String,
    about: String,
    category: String,
    subcategory: String,
    url: String,
    images: [String],
    is_deleted: { type: Boolean, default: false }
}, {
	timestamps: true
});

module.exports = mongoose.model('Product', ProductSchema);