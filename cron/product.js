'use strict';

// External files import
const category = require('../controllers/category.js');
const sub_category = require('../controllers/sub_category.js');
const pagination = require('../controllers/pagination.js');
const product_list = require('../controllers/product_list.js');
const product_detail = require('../controllers/product_detail.js');

// Models
const Product = require('../models/product.js');
const Category = require('../models/category.js');

// init = async () => {
//     const data = await Category.find({});
//     for (let i = 0; i < data.length; i++) {
//         console.log('category', data[i].category, 'subcategory', data[i].subcategory)
//         const cat = 'Computers';
//         const s_cat = 'COMPUTER COMPONENTS';
//         let cat_url, sub_cat_url;
//         if(cat){
//             cat_url = await category.init('https://www.amazon.com', cat);
//         }
//         if(s_cat){
//             sub_cat_url = await sub_category.init(cat_url, s_cat);
//         }
//         var url = cat && !s_cat ? cat_url : sub_cat_url;
//         var flag = cat && !s_cat ? false : true;
//         if (url) {
//             var pages = await pagination.init(url, flag);
//             for (const page_url of pages) {
//             const products_list = await product_list.call(page_url, flag);
//             await productDetail(products_list, req.query);
//             }
//         } else {
//             console.log("url", url)
//         }
//     }
// };

productDetail = async (products_list, body) => {
    if(products_list.length !== 0 ) {
      for (const product of products_list){
        var data = await Product.findOne({ 'title':product.title });
        console.log("data", data === null ? 'Not Availabale': 'Available')
        if(data === null){
          var productDetail = await product_detail.call(product.url);
          productDetail.category = body.category;
          productDetail.subcategory = body.subcategory;
          productDetail.url = product.url;
          console.log("productDetail", productDetail)
          if(productDetail.title) {
            await insertInDB(productDetail);
          }
        } else if(!data.subcategory && body.subcategory) {
          data.subcategory = body.subcategory;
          await updateInDB(data);
        }
      }
    }
}


insertInDB = async (productObject) => {
    // Create a Product
   const product = new Product({
     title: productObject.title,
     price: productObject.price,
     about: productObject.about,
     category: productObject.category || null,
     subcategory: productObject.subcategory || null,
     url: productObject.url,
     images: productObject.images,
   });
 
   // Save Product in the database
   await product.save()
     .then(data => {
         // res.send(data);
         return data;
       }).catch(err => {
         console.log("err", err)
       // res.status(500).send({
       //     message: err.message || "Some error occurred while creating."
       // });
     });
 }
 
 updateInDB = async (productObject) => {
  // Save Product in the database
  await productObject.save()
    .then(data => {
        // res.send(data);
        return data;
      }).catch(err => {
        console.log("err", err)
      // res.status(500).send({
      //     message: err.message || "Some error occurred while creating."
      // });
    });
 }

 (async() =>  {
    const data = await Category.find({});
    for (let i = 0; i < data.length; i++) {
        console.log('category', data[i].category, 'subcategory', data[i].subcategory)
        const cat = 'Computers';
        const s_cat = 'COMPUTER COMPONENTS';
        let cat_url, sub_cat_url;
        if(cat){
            cat_url = await category.init('https://www.amazon.com', cat);
        }
        if(s_cat){
            sub_cat_url = await sub_category.init(cat_url, s_cat);
        }
        var url = cat && !s_cat ? cat_url : sub_cat_url;
        var flag = cat && !s_cat ? false : true;
        if (url) {
            var pages = await pagination.init(url, flag);
            for (const page_url of pages) {
            const products_list = await product_list.call(page_url, flag);
            await productDetail(products_list, req.query);
            }
        } else {
            console.log("url", url)
        }
    }
})();