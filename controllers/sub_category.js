const puppeteer = require('puppeteer');

exports.init = async (url, selectedSubCategory) => {
    const browser = await puppeteer.launch({
        headless: true,
        args: ['--no-sandbox', '--disable-extensions'],
    });
    const page = await browser.newPage();
    page.setViewport({ width: 1280, height: 926 });
    await page.goto(url, {waitUntil: 'load'});
    let subCategoriesObject;
    let sub_cat_url;
    try {
        subCategoriesObject = await page.evaluate(() => {
            var sub_cat_result = document.querySelectorAll('.acswidget .a-row .acs_tile a');
            let sub_categories = [];
            for (i = 0; i < sub_cat_result.length; i++) {
                sub_categories.push({ 'text':sub_cat_result[i].innerText.trim() , 'url': sub_cat_result[i].href });
            }
            return sub_categories;
        });
        await page.waitFor(5000);
        subCategoriesObject.map((obj) => { if(obj.text === selectedSubCategory){ sub_cat_url = obj.url } });
    } catch(e) {
        console.log("e ==-=-=-=-=-=--==---", e)
    }
    await browser.close();
    console.log("sub_cat_url", sub_cat_url);
    return sub_cat_url;
};

// (() => init("https://www.amazon.com/s/browse?_encoding=UTF8&node=16225007011&ref_=nav_shopall-export_nav_mw_sbd_intl_computers", "COMPUTER ACCESSORIES"))();

