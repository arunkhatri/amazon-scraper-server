const puppeteer = require('puppeteer');

exports.init = async (url, isSubcategory) => {
    const browser = await puppeteer.launch({
        headless: true,
        args: ['--no-sandbox', '--disable-extensions'],
    });
    const page = await browser.newPage();
    page.setViewport({ width: 1280, height: 926 });
    await page.goto(url, {waitUntil: 'load', timeout: 0});
    let pages_urls;
    try {
        if(!isSubcategory) {
            pages_urls = await page.evaluate(() => {
                var pagination = document.querySelectorAll('#pagn')[0].innerText;
                var arrayOfPagination = pagination.trim().split(" ");
                var total = arrayOfPagination[arrayOfPagination.length-3];
                var urls = [];
                for (i = 0; i < total; i++) {
                    urls.push(document.querySelectorAll('body')[0].baseURI+'&page='+(i+1));
                  }
                return urls;
            })
            await page.waitFor(5000);
        } else {
            pages_urls = await page.evaluate(() => {
                var pagination = document.querySelectorAll('.a-pagination');
                if(pagination){
                    var pages = pagination[0].innerText.split('\n');
                    var total = pages[pages.length - 2];
                    var urls = [];
                    for (i = 0; i < total; i++) {
                        if(i==0){
                            urls.push(document.querySelectorAll('body')[0].baseURI);
                        } else {
                            urls.push(document.querySelectorAll('body')[0].baseURI+'&page='+(i+1));
                        }
                    }
                    return urls;
                } else {
                    var cat_pagination = document.querySelectorAll('#pagn')[0].innerText;
                    var arrayOfPagination = cat_pagination.trim().split(" ");
                    var total = arrayOfPagination[arrayOfPagination.length-3];
                    var urls = [];
                    for (i = 0; i < total; i++) {
                        urls.push(document.querySelectorAll('body')[0].baseURI+'&page='+(i+1));
                    }
                    return urls;
                }
            })
            await page.waitFor(5000);
        }
    } catch(e) {
        console.log("Pagination.js Page ==-=-=-=-=-=--==---", e)
        return;
    }
    await browser.close();
    console.log("pages_urls", pages_urls.length);
    return pages_urls;
};

// (() => init("https://www.amazon.com/s/browse?_encoding=UTF8&node=16225007011&ref_=nav_shopall-export_nav_mw_sbd_intl_computers", false))();
// (() => init("https://www.amazon.com/s/ref=s9_acss_bw_cts_Computer_T1_w?fst=as%3Aoff&rh=n%3A16225007011%2Cn%3A172456&bbn=16225007011&ie=UTF8&qid=1487012920&rnid=16225007011&pf_rd_m=ATVPDKIKX0DER&pf_rd_s=merchandised-search-4&pf_rd_r=EPNHRP476TVFC4M88QN2&pf_rd_t=101&pf_rd_p=74069509-93ef-4a3c-8dca-a9e3fa773a64&pf_rd_i=16225007011", true))();

