const puppeteer = require('puppeteer');

function extractAnswers() {
    var title = document.querySelectorAll('#productTitle')[0].innerText; // Title
    var temp_price = document.querySelectorAll('#priceblock_ourprice')[0]; // Price
    if(!temp_price){
        temp_price = document.querySelectorAll('#buyNew_noncbb')[0];
    } 
    if(!temp_price){
        temp_price = document.querySelectorAll('#price_inside_buybox')[0];
    }
    var price = !temp_price ? null: temp_price.innerText;
    var about = document.querySelectorAll('#feature-bullets')[0].innerText; // About
    var images_array = document.querySelectorAll('#altImages ul li:not(.aok-hidden) img'); 
    var images=[];
    for (i = 0; i < images_array.length; i++) {
        images.push(images_array[i].src.replace("._SS40_", ""));
    }
    return { title, price, about, images};
}

scrapeInfiniteScrollAnswers = async ( page, extractAnswers) => {
    let product_detail;
  try {
    product_detail = await page.evaluate(extractAnswers);
    // console.log("product_detail", product_detail);
  } catch(e) {
    console.log("e ==-=-=-=-=-=--==---", e)
  }
  return product_detail;
}

exports.call = async (url) => {
  console.log("url", url)
  const browser = await puppeteer.launch({
    headless: true,
    args: ['--no-sandbox', '--disable-extensions'],
  });
  const page = await browser.newPage();
  page.setViewport({ width: 1280, height: 926 });
  await page.goto(url, {waitUntil: 'load'});
  const data = await scrapeInfiniteScrollAnswers(page, extractAnswers);
  await browser.close();
  return data;
};

// callQuestions('https://www.amazon.com/Samsung-MicroSDXC-Adapter-MB-ME128GA-AM/dp/B06XWZWYVP/ref=lp_16225007011_1_2?s=computers-intl-ship&ie=UTF8&qid=1566242382&sr=1-2');
