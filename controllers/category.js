const puppeteer = require('puppeteer');

exports.init = async (url, category) => {
    const c_url = await getUrl(url, category);
    console.log("cat_url", c_url);
    return c_url;
};

getUrl = async (url, category) => {
    console.log(category);
    const browser = await puppeteer.launch({
        headless: true,
        args: ['--no-sandbox', '--disable-extensions'],
    });
    const page = await browser.newPage();
    page.setViewport({ width: 1280, height: 926 });
    await page.goto(url, {waitUntil: 'load', timeout: 0});
    let categoriesObject;
    let cat_url;
    try {
        categoriesObject = await page.evaluate(() => {
            var cat_result = document.querySelectorAll('.nav-catFlyout .nav-flyout-content a');
            let categories = [];
            for (i = 0; i < cat_result.length; i++) {
                categories.push({ 'text':cat_result[i].text.trim() , 'url': cat_result[i].href });
            }
            return categories;
        });
        cat_url = await categoriesObject.find((obj) => { if(obj.text === category){ return obj.url } });
        await page.waitFor(5000);
        if(!cat_url){
            categoriesObject.forEach(element => {
                if(element.text === category){ cat_url= element; }
            });
        }
    } catch(e) {
        console.log("e ==-=-=-=-=-=--==---", e)
    }
    await browser.close();
    // if(!cat_url){
    //     getUrl("https://www.amazon.com", category);
    // } else {
    // }
    return cat_url.url;
}

// (() => init("https://www.amazon.com", "Computers"))();