const puppeteer = require('puppeteer');

function category() {
    var results =document.querySelectorAll('#search-results ul.s-result-list li.s-result-item a.a-link-normal.s-access-detail-page.s-color-twister-title-link.a-text-normal');
    var products=[];
    for (i = 0; i < results.length; i++) {
        products.push( { url: results[i].href, title: results[i].innerText });
    }
    return products;
}

function subcategory() {
    var results = document.querySelectorAll('.s-search-results .s-result-item h2>a.a-link-normal.a-text-normal');
    var product=[];
    for (i = 0; i < results.length; i++) {
        product.push({url: results[i].href, title: results[i].innerText});
    }
    return product;
}

scrapeInfiniteScrollAnswers = async ( page, scrapFunction) => {
  let product_urls;
  try {
    product_urls = await page.evaluate(scrapFunction);
  } catch(e) {
    console.log("e ==-=-=-=-=-=--==---", e)
  }
  return product_urls;
}
// exports.
exports.call = async (url, isSubcategory) => {
  console.log("url", url, "isSubcategory", isSubcategory)
  const browser = await puppeteer.launch({
    headless: true,
    args: ['--no-sandbox', '--disable-extensions'],
  });
  const page = await browser.newPage();
  page.setViewport({ width: 1280, height: 926 });
  await page.goto(url, {waitUntil: 'load'});
  const product_urls = await scrapeInfiniteScrollAnswers(page, isSubcategory ? subcategory : category);
  await browser.close();
  console.log("product_urls", product_urls.length);
  return product_urls;
};

// call('https://www.amazon.com/s/browse?_encoding=UTF8&node=16225007011&ref_=nav_shopall-export_nav_mw_sbd_intl_computers', false);
// call('https://www.amazon.com/s?bbn=16225007011&rh=n%3A16225007011%2Cn%3A172456&dc&fst=as%3Aoff&pf_rd_i=16225007011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=74069509-93ef-4a3c-8dca-a9e3fa773a64&pf_rd_r=V6EVKMGZWW8PJ28TXAC6&pf_rd_s=merchandised-search-4&pf_rd_t=101&qid=1487012920&rnid=16225007011&ref=s9_acss_bw_cts_Computer_T1_w', true);
